# README #
This is the (opinionated) repo for [Crux Linux](https://crux.nu).
It contains the ports for various virtualisation
applications, most notable being

* [libvirt](https://libvirt.org)
* [guestfs](libguestfs.org)
* [virt-manager](https://virt-manager.org)

It has quite a few ports, because all these applications
have quite a few dependencies.

### Usage ###

* Clone this repo somewhere
* `cd $repo && git co 3.3` (3.3 branch is where all the most recent stuff is)
* Edit `/etc/prt-get.conf` to include it
* Run `prt-get cache`
* **Profit!**

### **BIG FAT WARNING!!** ###

There is **absolutely no guarantee** that it will not blow up your computer,
eat your kittens or gain sentience and take over the world.

**tl;dr**
If you break something, fix it and send me the patch :)

### TODO ###
* Problematic ports
    * None (famous last words...)

* Refine dependencies
    * Some now mandatory dependencies might be optional.Needs testing.

* Test install on a clean 3.3 system (DONE, in a Docker container)

### FAQ ###

**Q**:The repo is quite big for bunch of text.Why?

> Short answer: because I suck at git.
>
> Longer answer: I included some tarballs generated from git snapshots in the past.
> Most of them are since removed, but git still remembers them.

**Q**:What's with the repo name?Do you even know how to spell, bro?

> It's an alternative (British, iirc) spelling.
> Now, I am *not* English, nor do I play one on the TV, but I prefer this one, for some reason.

### Thanks ###

Some of the ports were stolen^Wborrowed from various people/entities.
~~I left the `Packager:` line intact.If I forgot some, appologies.~~

### Contact ###

You can ask questions, yell at me for breaking your system or laugh
at me for stupid things I've done at:

* Freenode `#crux`

* the main Crux mailing list

Enjoy.

:wq
