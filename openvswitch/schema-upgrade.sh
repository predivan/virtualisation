#!/bin/bash

CONFDB=/var/lib/openvswitch/conf.db
CONFDB_BACKUP=/var/lib/openvswitch/conf.db.bak
DBSCHEMA=/usr/share/openvswitch/vswitch.ovsschema

if [ -f $CONFDB ]
then
    cp $CONFDB $CONFDB_BACKUP
    ovsdb-tool convert $CONFDB $DBSCHEMA
    printf \n "$CONFDB converted,verify before usage."
    exit 0
else
    ovsdb-tool create $CONFDB $DBSCHEMA
fi
